#coding: utf-8
namespace :example do

	task :loop_news => :environment do
		puts "Start looping news"
		i=0
		loop do
			i+=1
			news = Article.create(title:"Руководство московского метро обязали ездить на работу в подземке ",preview:"Руководители московского метро теперь обязаны ездить на работу в подземке вместе с другими пассажирами. Об этом заявил начальник столичного метрополитена Дмитрий Пегов, передает ТАСС.",body:"Он сообщил, что недавно подписал приказ, согласно которому все его заместители и начальники служб должны пользоваться метрополитеном, чтобы добираться до места работы. «Нужно лично видеть и понимать, что происходит в вверенном подразделении, как ведется работа, что необходимо улучшить или переделать», — заявил Пегов.

Сам глава московской подземки, по его признанию, ездит на работу именно так, проводя под землей около 35 минут и делая пересадку с одной линии на другую. При этом Пегов подчеркнул, что правом проезда в кабине машиниста он пользуется редко, предпочитая ехать как обычный пассажир.",published_at: Time.now, published_to: Time.now+10.days, user_id: 2, types:0)
			break if i==10
		end
	end
	task :loop_info => :environment do
		puts "Start looping info"
		i=0
		loop do
			i+=1
			news = Article.create(title:"Пример полезной информации",preview:"Пример превью полезной информации",body:"Пример текста полезной информации",published_at: Time.now, published_to: Time.now+10.days, user_id: 2, types: 1)
			break if i==5
		end
	end
	task :loop_docs => :environment do
		puts "Start looping docs"
		i=0
		loop do
			i+=1
			news = Article.create(title:"Приказ",preview:"Какой-то приказ",body:"Текст документа",published_at: Time.now, published_to: Time.now+10.days, user_id: 2, types: 3)
			break if i==5
		end
	end
	task :loop_guides => :environment do
		puts "Start looping guides"
		i=0
		loop do
			i+=1
			news = Article.create(title:"Инструкция #"+rand(1...100).to_s,preview:"Сегодня я расскажу вам как делать что-то",body:"1. Купите что-то, 2. ???, 3.PROFIT",published_at: Time.now, published_to: Time.now+10.days, user_id: 2, types: 2)
			break if i==5
		end
	end
	task :loop_files => :environment do
		puts "Start looping files"
		i=0
		a = ["Картинка", "Документ","Видео","Приложение"]
		b = [".jpg",".png",".docx",".doc",".avi",".mp4",".wmv",".exe"]
		loop do
			i+=1
			news = Article.create(title: a.sample+b.sample,preview:"Файл"+" "+a.sample+b.sample,body:"Файл"+" "+a.sample+b.sample,published_at: Time.now, published_to: Time.now+10.days, user_id: 2, types: 4)
			break if i==5
		end
	end
	task :loop_videos => :environment do
		puts "Start looping videos"
		i=0
		loop do
			i+=1
			news = Article.create(title:"Видеоинструкция: "+[*"а".."я"].sample,preview:"картинка-обложка",body:"видео",published_at: Time.now, published_to: Time.now+10.days, user_id: 2, types: 5, vid_url: "http://www.youtube.com/embed/8ZcmTl_1ER8")
			break if i==5
		end
	end
	task :loop_map_data => :environment do
		puts "Start looping map data"
		i=0
		loop do
			i+=1
			news = MapData.create(city_id:1,date:Date.today, primary_dr_ticket: 1337,secondary_dr_ticket:1773)
			break if i==5
		end
	end
	task :map_data => :environment do
		puts "Start"
			cities = City.all
			cities.each do |city|
				r = Random.new
				primary = r.rand(0...12000)
				MapData.create(city_id: city.id ,date: Date.today, primary_dr_ticket: primary,secondary_dr_ticket: 0)
			end
		puts "Stop"
	end
	task :map_data1 => :environment do
		puts "Start"
			cities = City.all
			cities.each do |city|
				r = Random.new
				primary = r.rand(0...12000)
				MapData.create(city_id: city.id ,date: Date.today-1.day, primary_dr_ticket: primary,secondary_dr_ticket: 0)
			end
		puts "Stop"
	end
end