# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Organization.destroy_all
InfoDesk.destroy_all
User.destroy_all
Department.destroy_all
org = Organization.create(address:'ул. Вишневского д.1',name:'МИАЦ',city_id: 13,city_code:'4842')
pd = Department.create(name:'Администрация')
pd_pr = Department.create(name:'Отдел программирования')

a = User.create(username: 'antonov', password: 'XAfb32Mt', email: '0@ru.com', parent_id: nil)
InfoDesk.create(user_id:a.id,contact_tel:'123456',contact_email:'test@test.com',position:'test',organization_id:org.id,department_id:pd.id,created_at:Time.now,updated_at:Time.now)
admin = User.create(username: 'admin', password: '12345678', email: 'seleznev.miac@gmail.ru', parent_id: a.id)
InfoDesk.create(user_id:admin.id,contact_tel:'123456',contact_email:'test@test.com',position:'test',organization_id:org.id,department_id:pd_pr.id,created_at:Time.now,updated_at:Time.now)
User.create(username: 'banned', password: '12345678', email: 'okimbanned@gmail.com', parent_id: User.last.id, fio: 'asd', blocks: true)