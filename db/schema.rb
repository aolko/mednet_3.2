# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20141224080953) do

  create_table "articles", :force => true do |t|
    t.string   "title"
    t.integer  "types"
    t.string   "location"
    t.text     "preview"
    t.text     "body"
    t.datetime "published_at"
    t.datetime "published_to"
    t.integer  "user_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "vid_url"
  end

  create_table "blocks", :force => true do |t|
    t.integer  "user_id",                              :null => false
    t.string   "reason",       :default => "Blocked."
    t.datetime "blocked_at"
    t.datetime "unblocked_at"
  end

  create_table "cities", :force => true do |t|
    t.string   "lsd_ids",                           :null => false
    t.string   "map_id",                            :null => false
    t.string   "name",       :default => "Город"
    t.string   "district",   :default => "Область"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
  end

  create_table "ckeditor_assets", :force => true do |t|
    t.string   "data_file_name",                  :null => false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    :limit => 30
    t.string   "type",              :limit => 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], :name => "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], :name => "idx_ckeditor_assetable_type"

  create_table "departments", :force => true do |t|
    t.string "name",                  :null => false
    t.string "desc",  :default => ""
    t.string "phone", :default => ""
  end

  create_table "info_desks", :force => true do |t|
    t.integer  "user_id",         :null => false
    t.string   "contact_tel",     :null => false
    t.string   "contact_email",   :null => false
    t.string   "private_tel"
    t.string   "private_email"
    t.string   "position",        :null => false
    t.integer  "organization_id", :null => false
    t.integer  "department_id",   :null => false
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "map_data", :force => true do |t|
    t.integer  "city_id",             :null => false
    t.datetime "date"
    t.integer  "primary_dr_ticket"
    t.integer  "secondary_dr_ticket"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "messages", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "organizations", :force => true do |t|
    t.string  "address",       :null => false
    t.integer "city_id",       :null => false
    t.string  "name",          :null => false
    t.text    "description"
    t.string  "logo"
    t.string  "tel_secretary"
    t.string  "fax"
    t.string  "city_code",     :null => false
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "roles_users", :id => false, :force => true do |t|
    t.integer "role_id"
    t.integer "user_id"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0,     :null => false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "username"
    t.boolean  "admin",                  :default => false
    t.boolean  "moderator",              :default => false
    t.boolean  "boolean",                :default => false
    t.string   "fio"
    t.string   "hospital"
    t.boolean  "blocks",                 :default => false
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.string   "ancestry"
    t.boolean  "top_manager",            :default => false
    t.boolean  "middle_person",          :default => false
    t.integer  "role_id",                :default => 4
  end

  add_index "users", ["ancestry"], :name => "index_users_on_ancestry"
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
