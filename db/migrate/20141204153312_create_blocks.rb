class CreateBlocks < ActiveRecord::Migration
	def change
		create_table :blocks do |t|
			t.integer :user_id, null:false
			t.string :reason, default: "Blocked."
			t.timestamp :blocked_at
			t.timestamp :unblocked_at
		end
	end
end
