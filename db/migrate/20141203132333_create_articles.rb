class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
    	t.string :title
    	t.integer :types
    	t.string :location
      t.text :preview
    	t.text :body
    	t.timestamp :published_at
    	t.timestamp :published_to
    	t.integer :user_id
      t.timestamps
    end
  end
end
