class CreateInfoDesks < ActiveRecord::Migration
  def change
    create_table :info_desks do |t|
      t.integer :user_id, null:false
      t.string :contact_tel, null:false
      t.string :contact_email, null:false
      t.string :private_tel
      t.string :private_email
      t.string :position, null:false
      t.integer :organization_id, null:false
      t.integer :department_id, null:false
      t.timestamps
    end
  end
end
