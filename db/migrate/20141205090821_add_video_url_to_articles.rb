class AddVideoUrlToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :vid_url, :string
  end
end
