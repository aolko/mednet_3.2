class CreateMapData < ActiveRecord::Migration
  def change
    create_table :map_data do |t|
			t.integer :city_id, null:false
			t.timestamp :date
			t.integer :primary_dr_ticket
			t.integer :secondary_dr_ticket

      t.timestamps
    end
  end
end
