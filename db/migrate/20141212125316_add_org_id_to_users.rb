class AddOrgIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :top_manager, :boolean, default: false
    add_column :users, :middle_person, :boolean, default: false
  end
end
