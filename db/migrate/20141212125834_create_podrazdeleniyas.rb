class CreatePodrazdeleniyas < ActiveRecord::Migration
  def change
    create_table :departments do |t|
      t.string :name, null:false
      t.string :desc, default:""
      t.string :phone, default: ""
    end
  end
end
