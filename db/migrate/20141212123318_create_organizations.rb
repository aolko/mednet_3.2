class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.string :address, null:false
      t.integer :city_id, null:false
      t.string :name, null:false
      t.text :description
      t.string :logo
      t.string :tel_secretary
      t.string :fax
      t.string :city_code, null:false
    end
  end
end
