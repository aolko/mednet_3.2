class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
			t.string :lsd_ids, null:false
			t.string :map_id, null:false
			t.string :name, default:"Город"
			t.string :district, default:"Область"

      t.timestamps
    end
  end
end
