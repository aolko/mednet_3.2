class UsersController < ApplicationController
	load_and_authorize_resource
  before_filter :authenticate_user!
  before_filter :verify_admin

  def verify_admin
	  :authenticate_user!
	  redirect_to root_url unless has_role?(current_user, 'Администратор')
  end

  def current_ability
	  @current_ability ||= UserAbility.new(current_user)
  end

  def Index

  end
	def profile
		@user = current_user
		if params[:commit] == "Сохранить"
			if params[:fio].blank?
				redirect_to profile_user_path(@user.id, change: "fio"), alert:t('error.err_should_not_be_empty', :field => 'ФИО')
			else
				User.change_fio(params,current_user)
				redirect_to profile_user_path(@user.id),notice:t('notice.notice_saved',:suffix => 'но', :field => 'ФИО')
			end
		end
		if params[:commit] == "Изменить пароль"
			if params[:new_password].blank?
				redirect_to profile_user_path(@user.id, change: "new_password"), alert:t('error.err_should_not_be_empty', :field => 'Новый пароль')
			else
				User.change_password(params,current_user)
				redirect_to profile_user_path(@user.id),notice:t('notice.notice_saved',:suffix => 'н', :field => 'Новый пароль')
			end
		end
	end
	def blocked
		users = User.where(blocks: true)

	end
	def manage
		@roles = Role.all

		if current_user.blank?
			redirect_to({controller: 'main', action: 'index'},alert: t('error.err_no_access'))
		else
			redirect_to({controller: 'main', action: 'index'},alert: t('error.err_no_access')) unless has_role?(current_user, 'Администратор')
		end

		@users = User.all
		if params[:blocks] == "true"
			User.unblocked_user(params)
			redirect_to manage_users_path,notice:"Пользователь #{User.find(params[:id]).username} разблокирован"
		end
		if params[:blocks] == "false"
			User.blocked_user(params)
			redirect_to manage_users_path,notice:"Пользователь #{User.find(params[:id]).username} заблокирован"
		end

	end
	def adduser
		@roles = Role.all

		unless current_user
			redirect_to({controller: 'main', action: 'index'},alert: t('error.err_no_access'))
		else
			redirect_to({controller: 'main', action: 'index'},alert: t('error.err_no_access')) unless has_role?(current_user, 'Администратор')
		end

		User.add_user(params)
		redirect_to adduser_users_path,notice:"Пользователь добавлен"
	end


end
