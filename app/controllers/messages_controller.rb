class MessagesController < ApplicationController
	respond_to :html

	def index
	end

	def create
		message = Message.new(params[:contact_form])
		if message.deliver
			redirect_to root_path, :notice => 'Email был отослан.'
		else
			redirect_to root_path, :notice => 'Email не может быть отослан.'
		end
	end

end