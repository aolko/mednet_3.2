class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def has_role?(current_user,role)
	  return !!current_user.roles.find_by_name(role.to_s.camelize)
  end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
	  # redirect_to root_url, :alert => exception.message
  end

  # def authenticate_users!
	 #  authenticate_user!  if !(%w{sessions main admins password}.include? controller_name)
	 #  # redirect_to new_user_session_path
  # end
end
