class MainController < ApplicationController
  def index
		sjson = MapData.select("cities.map_id as map_id, primary_dr_ticket as lsd_ids, cities.name as name").joins(:city)
		@json = sjson.today
		@mapdata = @json.limit(15)

		# @mapdata = City.select("map_id ,lsd_ids,name")
		if user_signed_in?
			if current_user.sign_in_count == 1
				respond_to do |format|
					format.html
					format.json { render json: @json }
				end
				# redirect_to({controller: 'users', action: 'profile', id: current_user.id}, notice:"Измените ваш пароль" )
			else
				respond_to do |format|
					format.html
					format.json { render json: @json }
				end
			end
		else
			respond_to do |format|
				format.html
				format.json { render json: @json }
			end
		end		
  end
end
