class ArticlesController < ApplicationController

	def index
		@articles = Article.paginate(:page => params[:page], :per_page => 50	)
		if params[:show].present?
			@articles = Article.where(id: params[:id])
		end
  end
  def helpful
	  @articles = Article.paginate(:page => params[:page], :per_page => 5).types_1
	  if params[:show].present?
		  @articles = Article.where(id: params[:id])
	  end
  end
  def instruction
	  @articles = Article.paginate(:page => params[:page], :per_page => 5).types_2
	  if params[:show].present?
		  @articles = Article.where(id: params[:id])
	  end
  end
  def archive
	  @articles = Article.paginate(:page => params[:page], :per_page => 5).types_4
	  if params[:show].present?
		  @articles = Article.where(id: params[:id])
	  end
  end
	def documents
		@articles = Article.paginate(:page => params[:page], :per_page => 5).types_3
		if params[:show].present?
			@articles = Article.where(id: params[:id])
		end
	end
	def videotuts
		@articles = Article.paginate(:page => params[:page], :per_page => 5).types_5
		if params[:show].present?
			@articles = Article.where(id: params[:id])
		end
	end
	def manage
		@news = Article.all
		@roles = Role.all

		if current_user.blank?
			redirect_to({controller: 'main', action: 'index'},alert: t('error.err_no_access'))
		else
			redirect_to({controller: 'main', action: 'index'},alert: t('error.err_no_access')) unless has_role?(current_user, 'Администратор')
		end

		if params[:act] == "edit"
			Article.edit_article(params)
			redirect_to controller: 'articles', action: 'edit', id: params[:id]
		end
		if params[:act] == "remove"
			Article.remove_article(params)
			redirect_to manage_articles_path,notice: "Запись удалена"
		end
		if params[:types].present?
			@news = Article.where(types: params[:types])
			if params[:types] == "123"
				@news = Article.all
			end
			flash[:notice] = "Найдено #{@news.count} #{Russian.p(@news.count.to_i,"запись","записи","записей")}"
		end
		if params[:search].present?
			@searchitems = Article.aaaa(params)
			@news = @searchitems
			flash[:notice] = "Найдено #{@news.count} #{Russian.p(@news.count.to_i,"запись","записи","записей")} по запросу #{params[:search]}"
		end
	end
	def edit
		@roles = Role.all

		if current_user.blank?
			redirect_to({controller: 'main', action: 'index'},alert: t('error.err_no_access'))
		else
			redirect_to({controller: 'main', action: 'index'},alert: t('error.err_no_access')) unless has_role?(current_user, 'Администратор')
		end

		if params[:id].present?
			@article = Article.find(params[:id])
		end
		if params[:commit].present?
			Article.update_article(params)
			redirect_to manage_articles_path,notice: "Запись изменена"
		end
	end
	def remove

	end
	def add
		@roles = Role.all

		if current_user.blank?
			redirect_to({controller: 'main', action: 'index'},alert: t('error.err_no_access'))
		else
			redirect_to({controller: 'main', action: 'index'},alert: t('error.err_no_access')) unless has_role?(current_user, 'Администратор')
		end

		if params[:commit].present?
			if params[:title].blank?
				redirect_to add_articles_path,alert:t('error.err_cant_add_article', :reason => 'Не заполнен заголовок')
			elsif params[:types] == '123'
				redirect_to add_articles_path,alert:t('error.err_cant_add_article', :reason => 'Неправильный тип')
			elsif params[:preview].blank?
				redirect_to add_articles_path,alert:t('error.err_cant_add_article', :reason => 'Не заполнена аннотация записи')
			elsif params[:body].blank?
				redirect_to add_articles_path,alert:t('error.err_cant_add_article', :reason => 'Не заполнено содержание записи')
			else
				Article.add_article(params,current_user)
			end
		end
	end
	def addvideo
		@roles = Role.all

		if current_user.blank?
			redirect_to({controller: 'main', action: 'index'},alert: t('error.err_no_access'))
		else
			redirect_to({controller: 'main', action: 'index'},alert: t('error.err_no_access')) unless has_role?(current_user, 'Администратор')
		end

		if user_signed_in?
			unless current_user.admin == true
				redirect_to({controller: 'main', action: 'index'},alert: t('error.err_no_access'))
			end
			if params[:commit].present?
				Article.add_article(params,current_user)
				redirect_to videos_path,notice: "Видео добавлено"
			end
		end
	end
end
