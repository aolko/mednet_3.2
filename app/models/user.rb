class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
	has_and_belongs_to_many :roles
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  attr_accessible :email, :username, :password, :parent_id, :ansestry, :fio, :moderator, :admin, :hospital, :blocks, :remember_me, :encrypted_password, :created_at, :sign_in_count, :updated_at, :role_id
  has_ancestry
	# before_save :setup_role


	def role?(role)
		return !!self.roles.find_by_name(role.to_s.camelize)
	end

	# Default role is "Registered"
	# def setup_role
	# 	if self.role_ids.empty?
	# 		self.role_ids = [4]
	# 	end
	# end

	def self.change_fio params, current_user
		current_user.update_attributes(fio: params[:fio])
	end

  def self.change_password params, current_user
	  current_user.update_attributes(password: params[:new_password], sign_in_count: User.find(current_user.id).sign_in_count+1) if User.find(current_user.id).password.present?
  end

	def self.blocked_user params
		User.find(params[:id]).update_attributes(blocks: true) if User.find(params[:id]).present?
		Block.create(user_id:params[:id], reason:"no reason", blocked_at:Time.now) if Block.where(user_id: params[:id],reason:"no reason", blocked_at: Time.now).present?
	end

  def self.unblocked_user params
	  User.find(params[:id]).update_attributes(blocks: false) if User.find(params[:id]).present?
		Block.where(user_id: params[:id], unblocked_at: nil).first.update_attributes(unblocked_at:Time.now) if Block.where(user_id: params[:id], unblocked_at: nil).first.present?
  end

	def self.add_user params
		User.create(email: params[:email], username: params[:username], password:"123456aA", fio:params[:fio], role_id:params[:role_id], created_at:Time.now, sign_in_count:0, updated_at:Time.now)
	end
end
