class Organization < ActiveRecord::Base
  attr_accessible :address, :city_id, :description, :logo, :name, :tel_secretary, :fax, :city_code
end
