class Message < MailForm::Base
	attribute :name,          :validate => true
	attribute :email,         :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
	attribute :subject,       :validate => true
	attribute :message,       :validate => true

	def headers
		{
				:subject => "Связь с mednet",
				:to => "lekufowo@lackmail.net",
				:from => %("#{name}" <#{email}>)
		}
	end
end
