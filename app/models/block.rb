class Block < ActiveRecord::Base
attr_accessible :user_id, :reason, :blocked_at, :unblocked_at
end
