class City < ActiveRecord::Base
  attr_accessible :map_id,:lsd_ids,:name,:district
  has_one :map_data, :dependent => :destroy
end
