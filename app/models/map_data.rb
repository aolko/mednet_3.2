class MapData < ActiveRecord::Base
  attr_accessible :city_id, :date, :primary_dr_ticket, :secondary_dr_ticket
  # db_magic :connection => :slave
  # MapData.table_name = "clients"
  belongs_to :city
  scope :today, where("date between ? and ?",Date.today.beginning_of_day, Date.today.end_of_day)
  scope :yesterday, where("date between ? and ?",Date.today.beginning_of_day-1.day, Date.today.end_of_day-1.day)
end
