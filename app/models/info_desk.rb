class InfoDesk < ActiveRecord::Base
  attr_accessible :contact_email, :contact_tel, :position, :private_email, :private_tel, :user_id, :organization_id, :department_id, :created_at, :updated_at
end
