// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require main
//= require bootstrap
//= require bootstrapValidator.min
//= require_directory ./plugins
//= require_tree .
//= require qtip2-jquery-rails
//= require dataTables/jquery.dataTables
//= require twitter/typeahead
//= require tinymce
//= require jquery.flexslider-min
//= require jquery.form


function diplay_hide (blockId2)
{
		if ($(blockId2).css('display') == 'none')
		{$(blockId2).animate({height: 'show'}, 500); $()}
		else
		{$(blockId2).animate({height: 'hide'}, 500);}
}

function print_this (ArticleID)
{
		$(ArticleID).printElement({printMode:'popup'});
}

// instantiate the bloodhound suggestion engine
var numbers = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('num'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		local: [
				{ num: 'one' },
				{ num: 'two' },
				{ num: 'three' },
				{ num: 'four' },
				{ num: 'five' },
				{ num: 'six' },
				{ num: 'seven' },
				{ num: 'eight' },
				{ num: 'nine' },
				{ num: 'ten' }
		]
});

// initialize the bloodhound suggestion engine
numbers.initialize();

// instantiate the typeahead UI
$('.sugbox .typeahead').typeahead(null, {
		hint: true,
		highlight: true,
		minLength: 1
//		displayKey: 'num',
//		source: numbers.ttAdapter()
},
{
		name: 'numbers',
		displayKey: 'num',
		// `ttAdapter` wraps the suggestion engine in an adapter that
		// is compatible with the typeahead jQuery plugin
		source: numbers.ttAdapter()
});